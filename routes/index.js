var express = require('express');
var router = express.Router();
var fs = require('fs');

/* GET home page. */
router.get('/', function(req, res, next) {
  loadEntriesPage( res );
});

function loadEntriesPage( res ){
  // Read the file and print its contents.
  var filename = process.argv[2];
  fs.readFile(filename, 'utf8', function(err, data) {
    
    if (err) throw err;
    data = data.split(/\r?\n/);

    var entries = [];
    for(var i = 0; i < data.length; i++){
      if(data[i] && data[i].length){
        console.log(data[i]);
        entries.push(data[i].replace(/\s/g,''));
      }
    }

    res.render('index', 
      { 
        title: 'EnerForms API',
        existingEntries: entries
      }
    );
  });
}

/* POST a new entry */
router.post('/api/entries', function(req, res){
  var email = req.body.email;
  var password = req.body.password;

  fs.appendFile('db.txt', email + '\n', function (err) {
    if (err) throw err;
    console.log("Succesful entry addition of email: ", email);
    
    res.render('entries', 
      { 
        title: 'EnerForms API',
        added: email
      }
    );
  });
  
})

module.exports = router;
