var express = require('express');
var router = express.Router();

/* GET entries listing. */
router.get('/', function(req, res, next) {
  res.render('entries', 
    { 
      title: 'EnerForms API'
    }
  );
});

module.exports = router;
