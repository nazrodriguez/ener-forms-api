# EnerForms API #

Este proyecto de prueba ofrece una interfaz para una API local que maneja datos de una **form** 
y los refleja en un archivo de texto "db.txt".

### Tecnologías/Metodologías principales usadas ###

* Base del proyecto: [Node.js](https://nodejs.org/en/)
* Framework: [ExpressJs](https://expressjs.com/)
* Motor de templating: [Jade](http://jade-lang.com/)

### Tecnologías/Metodologías secundarias usadas ###

* Librería reloader live del server: [NodeMon](nodemonhttps://nodemon.io/)
* CSS toolkit: [Spectre.css](https://picturepan2.github.io/spectre/)

### "Getting started" ###

* [Descargar e instalar Node.js](https://nodejs.org/en/download/)
* npm install -g nodemon
* Posicionar una terminal sobre el proyecto y correr: **npm install && npm start**
* Abrir browser en *localhost:3000*

### Guidelines ###

[Link al PDF](https://bitbucket.org/nazrodriguez/ener-forms-api/src/a43fea96c1418d6e9d2e775a2aa5fc4e592e8791/EnerformsAPIProjectGuidelines.pdf?at=master&fileviewer=file-view-default), presionar "View Raw".

### Autor ###

Nazareno A. Rodríguez